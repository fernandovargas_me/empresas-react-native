const config = {
    api: {
        url: 'https://empresas.ioasys.com.br/api/v1',
        urlBase: 'https://empresas.ioasys.com.br'
    }
};

export default config;