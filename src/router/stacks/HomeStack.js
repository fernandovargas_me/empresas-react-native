import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Home, Enterprise } from '../../screens';

function HomeStack() {
    const Stack = createStackNavigator();

    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Enterprise" component={Enterprise} />
        </Stack.Navigator>
    );
}

export default HomeStack;