import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomNavigation, BottomNavigationTab } from '@ui-kitten/components';

import { HomeStack, ProfileStack } from './stacks';

const { Navigator, Screen } = createBottomTabNavigator();

const BottomTabBar = ({ navigation, state }) => (
    <BottomNavigation
        selectedIndex={state.index}
        onSelect={(index) => navigation.navigate(state.routeNames[index])}>
            <BottomNavigationTab title='Home'/>
            <BottomNavigationTab title='Profile'/>
    </BottomNavigation>
);

const TabNavigator = () => (
    <Navigator tabBar={props => <BottomTabBar {...props} />}>
        <Screen name="Home" component={HomeStack} />
        <Screen name="Profile" component={ProfileStack} />
    </Navigator>
);

const MainNavigator = () => (
    <TabNavigator />
);

export default MainNavigator;