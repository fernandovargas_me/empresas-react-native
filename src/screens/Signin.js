import React, { useContext, useState } from 'react';
import { StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { Button, Icon, Input, Layout, Text } from '@ui-kitten/components';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NetInfo from "@react-native-community/netinfo";

import AuthContext from '../context/AuthContext';
import api from '../services/api';

import Loading from '../components/Loading';

const Signin = () => {
    const [email, setEmail] = useState('');
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const [password, setPassword] = useState('');
    const [secureTextEntry, setSecureTextEntry] = useState(true);

    const { signin: ctxSignin } = useContext(AuthContext);

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry);
    };

    const passwordIcon = props => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    );

    async function handleSubmit() {
        setLoading(true);
        const netinfo = await NetInfo.fetch();

        if (netinfo.isConnected) {
            try {
                const { headers, data: user } = await api.post('/users/auth/sign_in', { email, password });

                if (user.success) {
                    api.defaults.headers['access-token'] = headers['access-token'];
                    api.defaults.headers['client'] = headers['client'];
                    api.defaults.headers['uid'] = headers['uid'];

                    await AsyncStorage.setItem('access-token', headers['access-token']);
                    await AsyncStorage.setItem('client', headers['client']);
                    await AsyncStorage.setItem('uid', headers['uid']);
                    await AsyncStorage.setItem('user', JSON.stringify(user.investor));

                    const {id, investor_name, email} = user.investor;
                    ctxSignin({ id, investor_name, email });
                }
            } catch (error) {
                setError('Invalid username or password');
            }
        } else {
            setError('Without internet connection');
        }

        setLoading(false);
    }

    if (loading) {
        return <Loading />
    }

    return (
        <Layout style={styles.cardLogin}>
            <Input
                value={email}
                label='Login'
                placeholder='Enter your e-mail'
                onChangeText={nextValue => setEmail(nextValue)}
            />
            <Input
                value={password}
                label='Password'
                placeholder='Enter your password'
                accessoryRight={passwordIcon}
                secureTextEntry={secureTextEntry}
                onChangeText={nextValue => setPassword(nextValue)}
            />
            <Button
                disabled={!email || !password}
                style={styles.submitLogin}
                onPress={handleSubmit}>
                Signin
            </Button>

            { error &&
                <Text appearance="hint" style={styles.loginError} status='danger'>
                    {error}
                </Text>
            }
        </Layout>
    );
}

const styles = StyleSheet.create({
    cardLogin: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 20
    },

    loginError: {
        marginTop: 16
    },

    submitLogin: {
        marginTop: 16,
        width: '100%'
    }
});

export default Signin;