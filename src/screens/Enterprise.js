import React from 'react';
import { Avatar, Layout, Text } from '@ui-kitten/components';
import { Image } from 'react-native';


import { StyleSheet } from 'react-native';
import config from '../config';

const Enterprise = props => {
    const enterprise = props.route.params;

    return (
        <Layout style={styles.container}>
            <Image
                style={styles.image}
                source={{ uri: `${config.api.urlBase}/${enterprise.photo}` }}
            />
            <Text style={styles.title} category="h3">{enterprise.enterprise_name}</Text>
            <Text category="p1">{enterprise.enterprise_type.enterprise_type_name}</Text>
            <Text category="p2">{enterprise.city} - {enterprise.country}</Text>

            <Text style={styles.price} category="h6" status="primary" >$ {enterprise.share_price}</Text>
            <Text style={styles.description} category="p1" appearance="hint">{enterprise.description}</Text>
        </Layout>
    );
};

const styles = StyleSheet.create({
	container: {
		alignItems: 'center',
		flex: 1
	},

    image: {
        borderRadius: 40,
        margin: 12,
        height: 200,
        width: 200
    },

    price: {
        fontWeight: 'bold',
        marginTop: 8
    },

    description: {
        marginTop: 32,
        paddingHorizontal: 16,
        textAlign: 'left'
    }
});

export default Enterprise;