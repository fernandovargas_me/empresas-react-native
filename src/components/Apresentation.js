import AsyncStorage from '@react-native-async-storage/async-storage';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Icon } from '@ui-kitten/components';

const ArrowRightIcon = (props) => (
    <Icon {...props} name='arrowhead-right-outline'/>
);

function Apresentation({ setIsFirstTime }) {
    async function handleNextScreen() {
        await AsyncStorage.setItem('alreadyLaunched', 'true');
        setIsFirstTime(false);
    }

    return (
        <View style={styles.container}>
            <Text>Welcome to App!</Text>
            <StatusBar style="auto" />
            <Button
                appearance='ghost'
                status='danger'
                accessoryLeft={ArrowRightIcon}
                onPress={handleNextScreen}
            />

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default Apresentation;