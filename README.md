![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações sobre o projeto Empresas.

---

### Informações importantes

-   O arquivo **App_Empresas.postman_collection-NEW** foi necessário tendo em vista que o antigo é imcompatível com a versão atual do Postman.

-   Todos os endpoints da API foram utilizados, porém notei uma certa dificuldade em tornar possível ao usuário uma pesquisa eficiente para o tipo de _enterprise_, visto que o endpoint só permite pesquisa pelo código e me parece que para o usuário final seria mais útil uma pesquisa textual, sendo assim, para esta versão eu removi o parametro de pesquisa por tipo deixando funcional apenas a pesquisa por nome do _enterprise_.

### Bibliotecas

A seguir uma breve descrição sobre as bibliotecas usadas:

-   **@ui-kitten** (e dependências): biblioteca muito útil para components de interface.
-   **axios**: um cliente HTTP baseado em Promises para fazer requisições. Utilizado para organizar as requisições em API.
-   **@react-native-async-storage/async-storage**: lib para, através do local storage, armazenar dados locais de usuário e header
-   **@react-navigation** (e dependências): utilizado para organizar a estrutura de navegação do aplicativo
-   **yarn add @react-native-community/netinfo**: lib utilizada para verificar se app tem conexão com internet
-   **eslint** (e dependências): ferramenta de lint para js e jsx
-   **prettier** (e dependências): lib para padronização de código

Este projeto foi desenvolvido usando o **@expo** e inclui libs dependentes para o bom funcionamento. Também é importante considerar que o projeto foi criado através do `yarn create react-native-app`

### Instruções para rodar o projeto

Execute os comandos a seguir:

-   `yarn install` - Na raiz do projeto
-   `cd ios` - Para iPhone
-   `pod install` - Para iPhone
-   `yarn android` - Para rodar o aplicativo em Android
-   `yarn ios` - Para rodar o aplicativo em iOS

### Dados para Teste

-   Usuário de Teste: testeapple@ioasys.com.br
-   Senha de Teste : 12341234

### Dicas

-   No Postman existem alguns parâmetros no header que devem ser passados em todas requests exceto na de login, eles serão retornados no endpoint de login, nos headers da request.
-   Evite utilizar muitas bibliotecas que não sejam diretamente relacionadas ao build da aplicação. O uso das mesmas não esta vetado, mas seria interessante ver como você faz seus componentes :)

### Sugestões sobre o desafio?

-   Atualizar o arquivo de importação de endpoints.
-   Permitir que o endpoint que permita que com um único input seja possível pesquisar por nome, tipo ou descrição :D
-   Para poder implementar um conceito de scroll infinito seria bacana a api (get) retornar dados paginados isso tras um
    benefício de desempenho <3
